-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Erstellungszeit: 07. Feb 2017 um 21:18
-- Server-Version: 10.1.20-MariaDB-1~jessie
-- PHP-Version: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `bka`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `anonymous_sessions`
--

CREATE TABLE `anonymous_sessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `cookie` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `teamID` int(11) NOT NULL DEFAULT '-1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `valid` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `leafs`
--

CREATE TABLE `leafs` (
  `id` int(10) UNSIGNED NOT NULL,
  `hash` char(64) COLLATE utf8_unicode_ci NOT NULL,
  `parentID` int(10) NOT NULL DEFAULT '0',
  `targetID` int(10) NOT NULL,
  `teamID` int(11) NOT NULL DEFAULT '0',
  `foundOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cost` double NOT NULL DEFAULT '0',
  `secret` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `recover`
--

CREATE TABLE `recover` (
  `id` int(10) UNSIGNED NOT NULL,
  `teamID` int(11) NOT NULL DEFAULT '-1',
  `token` text COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `used` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `reserved_teamnames`
--

CREATE TABLE `reserved_teamnames` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `targets`
--

CREATE TABLE `targets` (
  `id` int(10) UNSIGNED NOT NULL,
  `hash` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` tinytext COLLATE utf8_unicode_ci,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `email` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `blocksSent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `blocksAccepted` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `costsMax` double NOT NULL DEFAULT '0',
  `costsSum` double NOT NULL DEFAULT '0',
  `seenLast` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastInsert` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `loggedIn` tinyint(1) NOT NULL DEFAULT '0',
  `isAnonymous` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `anonymous_sessions`
--
ALTER TABLE `anonymous_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cookie` (`cookie`);

--
-- Indizes für die Tabelle `leafs`
--
ALTER TABLE `leafs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hash` (`hash`);

--
-- Indizes für die Tabelle `recover`
--
ALTER TABLE `recover`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `reserved_teamnames`
--
ALTER TABLE `reserved_teamnames`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `targets`
--
ALTER TABLE `targets`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `anonymous_sessions`
--
ALTER TABLE `anonymous_sessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `leafs`
--
ALTER TABLE `leafs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `recover`
--
ALTER TABLE `recover`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `reserved_teamnames`
--
ALTER TABLE `reserved_teamnames`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `targets`
--
ALTER TABLE `targets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
