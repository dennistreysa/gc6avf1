<?php

	require_once(__DIR__."/include/helper/session.php");

	session_start();

	session_restore();

	if( !isset($_SESSION["teamID"]) ){
?>

<!DOCTYPE html>
<html lang="de">
	<head>
		<title>GC6AVF1 | Anonymer Login</title>

		<link rel="icon" href="img/favicon.png">

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- External includes -->
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
			<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css">
			
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
			<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
			<script src='https://www.google.com/recaptcha/api.js'></script>

		<!-- Local includes -->
			<link rel="stylesheet" href="css/login.css">
			
			<script src="js/login.js"></script>
			<script src="js/messages_de.js"></script>
	</head>

	<body>
		<div class="row">
			<div class="text-center" style="font-size:xx-small;">
				Dies ist eine private, nichtkommerzielle Webseite, die für einen <a href="https://de.wikipedia.org/wiki/Geocaching">Rätsel-Geocache</a> erstellt wurde! Dies ist keine offizielle Webseite des BKA!
			</div>
		</div>
		<div class="row">
			<div class="text-center">
				<img src="img/bka.png">
			</div>
		</div>
		<div class="row">
			<div class="text-center">
				<div class="logo">Anonymer Login</div>
				<div class="login-form-1">
					<form id="login_anon-form" class="text-left">
						<div class="etc-login-form">
							<p>Bitte beachten Sie, dass der anonyme Login rein per Cookie gespeichert wird!<br>Eine Wiederherstellung ist nicht möglich!</p>
						</div>
						<div class="login-form-main-message"></div>
						<div class="main-login-form">
							<div class="login-group">
								<div class="form-group reg_team-checkbox">
									<input type="checkbox" id="briefing" name="briefing">
									<label for="briefing">Ich habe an der <a href="briefing.html">Teambesprechung</a> teilgenommen</label>
								</div>
								<div class="form-group reg_team-checkbox">
									<input type="checkbox" id="privacy_policy" name="privacy_policy">
									<label for="privacy_policy">Ich stimme der <a href="briefing.html#dse" target="_blank">Datenschutzerklärung</a> zu und habe den <a href="briefing.html#ha" target="_blank">Haftungsausschluss</a> gelesen und verstanden!</label>
								</div>
								<div class="form-group">
									<div class="g-recaptcha" data-sitekey="6Lc4ZhUTAAAAAKYpxMcttNXFEqW3r5v3_wc6hRwF"></div>
								</div>
							</div>
							<button type="submit" data-callback="recaptchaCallback" class="login-button"><i class="fa fa-chevron-right"></i></button>
						</div>
						<div class="etc-login-form">
							<p>Doch ein Team anlegen? <a href="createTeam.php">Team hier anlegen!</a></p>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>

<?php
	}else{
		header("Location: index.php");
	}
?>