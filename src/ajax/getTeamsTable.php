<?php

	require_once(__DIR__."/../include/Db.class.php");
	require_once(__DIR__."/../include/constants.php");
	require_once(__DIR__."/../include/helper/session.php");

	session_start();

	session_restore();

	if($_SESSION["teamID"]){

		$returnValue = array();
		$returnValue["data"] = array();

		// Database Object
		$database = new Db();

		// Get teams
		$database->bind("status", BLOCK_STATUS_VALID);
		$teams = $database->query("SELECT	(CASE WHEN teams.isAnonymous = 1 THEN '-anonymous-' ELSE teams.name END) AS name,
											DATE(teams.created) AS created,
											blocksSent AS leafCount,
											costsMax AS maxCost,
											costsSum AS sumCost,
											blocksAccepted AS validLeafCount
										FROM teams
										WHERE teams.deleted = 0
										HAVING blocksSent > 0;");

		foreach($teams as $team){
			array_push($returnValue["data"], array(
								"name" => htmlentities($team["name"], ENT_QUOTES),
								"created" => $team["created"],
								"leafCount" => $team["leafCount"],
								"validLeafCount" => $team["validLeafCount"],
								"maxCost" => strval(round(floatval($team["maxCost"]), 3)),
								"sumCost" => strval(round(floatval($team["sumCost"]), 3)),
								"percent" => strval((floatval($team["validLeafCount"]) / 1000.0) * 100.0)
				));
		}

		// Answer
		header("Content-Type: application/json; charset=utf-8");
		echo json_encode($returnValue);
	}
?>