<?php

	require_once(__DIR__."/../include/helper/validation.php");
	require_once(__DIR__."/../include/helper/session.php");
	require_once(__DIR__."/../include/helper/cleanup.php");
	require_once(__DIR__."/../include/Db.class.php");
	require_once(__DIR__."/../include/constants.php");
	require_once(__DIR__."/../include/getData.php");

	$teamID = null;
	$teamName = null;
	$target = null;
	$leaf = null;
	$secret = null;
	$database = null;
	$completed = false;

	$returnValue = array();

	if( isset($_GET['n'])
		&& isset($_GET['p'])
		&& isset($_GET['t'])
		&& isset($_GET['l'])
		&& isset($_GET['s'])
		&& checkMetaTeamName($_GET['n'])
		&& checkMetaPassword($_GET['p'])
		&& checkMetaHash($_GET['t'])
		&& checkMetaHash($_GET['l'])
		&& isValidSecret($_GET['s']) ){

		// Request by selfmade miner

		$database = new Db();

		// Check if that team exists
		$database->bind("teamName", $_GET['n']);
		$team = $database->query("SELECT	id,
											name,
											password
										FROM teams
										WHERE name = :teamName
											AND deleted = 0
											AND isAnonymous = 0
										LIMIT 1;");

		if(sizeof($team) == 1){
			if(password_verify($_GET['p'], $team[0]["password"])){
				$teamID = $team[0]["id"];
				$teamName = $team[0]["name"];
				$target = strtolower($_GET['t']);
				$leaf = strtolower($_GET['l']);
				$secret = $_GET['s'];
			}
		}
	}else{

		// Request by browser

		session_start();

		session_restore(true, true);

		if(isset($_SESSION["teamID"])){

			$teamID = $_SESSION["teamID"];
			$teamName = $_SESSION["name"];

			// Check if data was sent and if it is JSON
			if( isXMLHTTPRequest()
				&& isset($_POST) 
					&& isJSON(file_get_contents("php://input")) ){

				$blockData = json_decode(file_get_contents("php://input"), true);

				if( checkMetaHash($blockData['target'])
					&& checkMetaHash($blockData['leaf'])
					&& isValidSecret($blockData['secret']) ){

					$target = strtolower($blockData['target']);
					$leaf = strtolower($blockData['leaf']);
					$secret = $blockData['secret'];
				}
			}else{
				$returnValue["status"] = "error";
				$returnValue["message"] = "Please POST data via JSON Object";
			}
		}
	}


	if( !empty($teamID)
		&& !empty($teamName)
		&& !empty($target)
		&& !empty($leaf)
		&& !empty($secret) ){

		$blocksAccepted = getValidBlocks($teamID, $database);
		$completed = ($blocksAccepted >= BLOCKS_NEEDED);

		// The hash the team should have calculated
		$hash = strtolower(hash('sha256', strtolower($leaf).$teamName.$secret));

		// Convert hash and target to bignum string
		$hash_bc = hexStr2BigFloat($hash);
		$target_bc = hexStr2BigFloat($target);

		// Check if target > hash
		if(bccomp($target_bc, $hash_bc) == 1){

			if(!$database){
				$database = new Db();
			}

			// Lock table
			$database->beginTransaction();

			// Check if target is (still) valid
			$database->bind("target", $target);
			$checkTarget = $database->query("SELECT	id,
													(SELECT max(id) FROM targets) AS maxID
												FROM targets
												WHERE hash = :target
												ORDER BY id DESC
												LIMIT 1;");

			if(sizeof($checkTarget) == 1){

				// Check if leaf is/was valid
				$database->bind("leaf", $leaf);
				$checkLeafValid = $database->query("SELECT	id,
															parentID,
															status
													FROM leafs
													WHERE hash = :leaf
													ORDER BY id DESC;");

				if( sizeof($checkLeafValid) >= 1 ){

					// The block status
					$blockStatus = 99;
					$targetID = -1;
					$parentID = -1;
					$cost = 1;

					// Check if target is current target
					if($checkTarget[0]["id"] == $checkTarget[0]["maxID"]){

						// Check if leaf is amongst latest leafs
						if( $checkLeafValid[0]["status"] == BLOCK_STATUS_VALID_PENDING ){
							
							// Valid combination found ;)

							$cost = bcdiv($target_bc, $hash_bc, 5);

							// Security check
							if(!$cost){
								$cost = 42;
							}
							
							$blockStatus = BLOCK_STATUS_VALID_PENDING;
							$targetID = $checkTarget[0]["id"];
							$parentID = $checkLeafValid[0]["id"];

							// Increase blocksAccepted-counter of team which holds the parent block
							$database->bind("id", $checkLeafValid[0]["id"]);
							$team = $database->query("SELECT	teamID,
																cost
														FROM leafs
														WHERE id = :id;");

							$database->bind("teamID", $team[0]["teamID"]);
							$database->bind("cost1", $team[0]["cost"]);
							$database->bind("cost2", $team[0]["cost"]);
							$updateQuery = $database->query("UPDATE	teams
																SET	blocksAccepted = blocksAccepted + 1,
																	costsSum = costsSum + :cost1,
																	costsMax = GREATEST(costsMax, :cost2)
																WHERE id = :teamID;");
	
							// Set parent block valid
							$database->bind("status", BLOCK_STATUS_VALID);
							$database->bind("id", $checkLeafValid[0]["id"]);
							$checkTarget = $database->query("UPDATE	leafs
																SET status = :status
																WHERE id = :id;");

							// Invalidate branched blocks
							$database->bind("status_invalid", BLOCK_STATUS_BRANCH);
							$database->bind("status_pending", BLOCK_STATUS_VALID_PENDING);
							$database->bind("id", $checkLeafValid[0]["id"]);
							$database->bind("parentID", $checkLeafValid[0]["parentID"]);
							$checkTarget = $database->query("UPDATE	leafs
																SET status = :status_invalid
																WHERE id != :id
																	AND parentID = :parentID
																	AND status = :status_pending;");

							$returnValue["status"] = "success";
							$returnValue["new_target"] = strtolower($target);
							$returnValue["new_leaf"] = strtolower($hash);
							$returnValue["valid_blocks"] = getValidBlocks($teamID, $database);
							
						}else{
							$blockStatus = BLOCK_STATUS_LEAF_OUTDATED;
							$targetID = $checkTarget[0]["id"];
							$parentID = $checkLeafValid[0]["id"];
							$returnValue["status"] = "error";
							$returnValue["message"] = "The leaf you used is out of date, please get the latest one!";
						}
					}else{
						$blockStatus = BLOCK_STATUS_TARGET_OUTDATED;
						$targetID = $checkTarget[0]["id"];
						$returnValue["status"] = "error";
						$returnValue["message"] = "The target you used is out of date, please get the latest one!";
					}

					$storeToDB = true;

					// Check if team already has such a block
					if($blockStatus == BLOCK_STATUS_TARGET_OUTDATED
						|| $blockStatus == BLOCK_STATUS_LEAF_OUTDATED ){
						$database->bind("status", $blockStatus);
						$database->bind("hash", $leaf);
						$database->bind("teamID", $teamID);
						$doubleCheck = $database->query("SELECT	id
															FROM leafs
															WHERE status = :status
																AND hash = :hash
																AND teamID = :teamID;");

						if( sizeof($doubleCheck) >= 1 ){
							$storeToDB = false;
						}
					}

					if($storeToDB){

						// increment counter
						$database->bind("teamID", $teamID);
						$team = $database->query("	UPDATE teams
														SET blocksSent = blocksSent + 1
														WHERE id = :teamID;");

						$database->bind("hash", $hash);
						$database->bind("parentID", $parentID);
						$database->bind("targetID", $targetID);
						$database->bind("teamID", $teamID);
						$database->bind("cost", $cost);
						$database->bind("secret", $secret);
						$database->bind("status", $blockStatus);
						$checkTarget = $database->query("INSERT INTO leafs (
																				hash,
																				parentID,
																				targetID,
																				teamID,
																				cost,
																				secret,
																				status
																			)
																			VALUES
																			(
																				:hash,
																				:parentID,
																				:targetID,
																				:teamID,
																				:cost,
																				:secret,
																				:status
																			);");
					}

					cleanUpDatabase($database);

				}else{
					$returnValue["status"] = "error";
					$returnValue["message"] = "Where did you get that leaf from?";
				}
			}else{
				$returnValue["status"] = "error";
				$returnValue["message"] = "Where did you get that target from?";
			}

			// unlock
			$database->executeTransaction();

		}else{
			$returnValue["status"] = "error";
			$returnValue["message"] = "The hash is not lower than the target!";
		}
	}else{
		$returnValue["status"] = "error";
		$returnValue["message"] = "Invalid data was sent";
	}

	$returnValue["completed"] = $completed;

	// Answer
	header("Content-Type: application/json; charset=utf-8");
	echo json_encode($returnValue);


	// inspired by http://php.net/manual/en/ref.bc.php#99130
	function hexStr2BigFloat($str){
		if(strlen($str) == 1) {
			return hexdec($str);
		} else {
			$remain = substr($str, 0, -1);
			$last = substr($str, -1);
			return bcadd(bcmul(16, hexStr2BigFloat($remain)), hexdec($last));
		}
	}
?>