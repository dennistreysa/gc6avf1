<?php

	require_once(__DIR__."/../include/Db.class.php");
	require_once(__DIR__."/../include/getData.php");
	require_once(__DIR__."/../include/constants.php");
	require_once(__DIR__."/../include/helper/session.php");
	require_once(__DIR__."/../include/helper/rand.php");
	require_once(__DIR__."/../include/helper/validation.php");

	$teamID = null;
	$teamName = "";
	$database = null;

	if( isset($_GET['n'])
		&& checkMetaTeamName($_GET['n'])
		&& isset($_GET['p'])
		&& checkMetaPassword($_GET['p']) ){

		// Request by selfmade miner
		
		$database = new Db();

		// Check if that team exists
		$database->bind("teamName", $_GET['n']);
		$team = $database->query("SELECT	id,
											name,
											password
										FROM teams
										WHERE name = :teamName
											AND deleted = 0
										LIMIT 1;");

		if(sizeof($team) == 1){
			if(password_verify($_GET['p'], $team[0]["password"])){
				$teamID = $team[0]["id"];
				$teamName = $team[0]["name"];
			}
		}
	}else{

		// Request by browser

		session_start();

		session_restore(true, true, true);

		if(isset($_SESSION["teamID"])){
			$teamID = $_SESSION["teamID"];
			$teamName = $_SESSION["name"];
		}
	}

	if($teamID){

		$returnValue = array();

		if(!$database){
			$database = new Db();
		}

		$returnValue["target"] = getCurrentTarget($database);
		$returnValue["leaf"] = getCurrentLeaf($teamID, $database);
		$returnValue["valid_blocks"] = getValidBlocks($teamID, $database);
		$returnValue["teamName"] = $teamName;

		// Answer
		header("Content-Type: application/json; charset=utf-8");
		echo json_encode($returnValue);
	}
?>