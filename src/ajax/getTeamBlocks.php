<?php

	require_once(__DIR__."/../include/Db.class.php");
	require_once(__DIR__."/../include/constants.php");
	require_once(__DIR__."/../include/helper/session.php");

	session_start();

	session_restore(true);

	if($_SESSION["teamID"]){

		$returnValue = array();

		// Database Object
		$database = new Db();

		// Get team blocks
		$database->bind("teamID1", $_SESSION["teamID"]);
		$database->bind("teamID2", $_SESSION["teamID"]);
		$database->bind("maxBlocks", MAX_TEAM_BLOCKS_IN_TABLE);
		$teams = $database->query("SELECT * FROM (
													SELECT	id,
															foundOn date,
															cost,
															status,
															(SELECT COUNT(*) FROM leafs WHERE teamID = :teamID1) AS blocksTotal
														FROM leafs
														WHERE teamID = :teamID2
														ORDER BY id DESC
														LIMIT :maxBlocks
												) t ORDER BY t.id ASC;");

		foreach($teams as $counter => $team){
			array_push($returnValue, array(
								"found" => $team["date"],
								"cost" => $team["cost"],
								"status" => $team["status"],
								"number" => ($team["blocksTotal"] > MAX_TEAM_BLOCKS_IN_TABLE ? ($team["blocksTotal"] - MAX_TEAM_BLOCKS_IN_TABLE + $counter + 1) : ($counter + 1) )
				));
		}

		// Answer
		header("Content-Type: application/json; charset=utf-8");
		echo json_encode($returnValue);
	}
?>