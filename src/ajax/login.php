<?php

	require_once(__DIR__."/../include/helper/validation.php");
	require_once(__DIR__."/../include/Db.class.php");
	require_once(__DIR__."/../include/constants.php");
	require_once(__DIR__."/../include/libs/recaptcha/recaptcha.php");

	session_start();

	// Check if data was sent and if it is JSON
	if( isXMLHTTPRequest()
			&& isset($_POST) 
			&& isJSON(file_get_contents("php://input")) ){
		$loginData = json_decode(file_get_contents("php://input"), true);

		$teamName = trim($loginData['name']);

		$returnValue = array();

		// Check if keys are set
		if( checkMetaTeamName($teamName)
				&& checkMetaPassword($loginData['password'])
				&& !empty($loginData['captcha']) ){

			// Database Object
			$database = new Db();

			// Database cleanup
			$database->query("DELETE FROM teams WHERE created < (now() - INTERVAL 1 MONTH) AND deleted = 1 AND loggedIn = 0;");
			$database->query("UPDATE teams SET deleted = 1 WHERE created < (now() - INTERVAL 1 DAY) AND loggedIn = 0;");

			// Check if team exists
			$database->bind("name", $teamName);
			$teams = $database->query("SELECT	id,
												password,
												loggedIn
											FROM teams
											WHERE name = :name
												AND deleted = 0
											LIMIT 1;");

			// Check if teams exists already
			if(sizeof($teams) == 1){
				
				$teamData = $teams[0];

				// Check password
				if(password_verify($loginData["password"], $teamData["password"])){
					// Check if password algorithm is still secure
					if(password_needs_rehash($teamData["password"], PASSWORD_DEFAULT)){
						$newPassword = password_hash($loginData["password"], PASSWORD_DEFAULT);
						$database->bind("teamID", $teamData["id"]);
						$database->bind("password", $newPassword);
						$stmt = $database->query("UPDATE teams SET password = :password WHERE id = :teamID;");
					}

					// Set loggedIn-flag and update seenLast value
					$database->bind("teamID", $teamData["id"]);
					$stmt = $database->query("UPDATE teams
												SET loggedIn = 1, seenLast = now()
												WHERE id = :teamID;");

					// Set session variables
					$_SESSION["teamID"] = $teamData["id"];
					$_SESSION["name"] = $teamName;

					$returnValue["status"] = "success";
				}else{
					usleep(2000000);
					$returnValue["status"] = "error";
					$returnValue["message"] = "Das angegebene Passwort stimmt nicht!";
				}
			}else{
				$returnValue["status"] = "error";
				$returnValue["message"] = "Ein Team mit diesem Namen konnte nicht gefunden werden!";
			}

			// Answer
			header("Content-Type: application/json; charset=utf-8");
			echo json_encode($returnValue);
		}
	}
?>