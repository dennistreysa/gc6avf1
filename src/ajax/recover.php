<?php

	require_once(__DIR__."/../include/helper/validation.php");
	require_once(__DIR__."/../include/helper/rand.php");
	require_once(__DIR__."/../include/Db.class.php");
	require_once(__DIR__."/../include/constants.php");
	require_once(__DIR__."/../include/libs/recaptcha/recaptcha.php");

	session_start();

	usleep(1000000);

	// Check if data was sent and if it is JSON
	if( isXMLHTTPRequest()
			&& isset($_POST) 
				&& isJSON(file_get_contents("php://input")) ){

		$returnValue = array();
		$teamData = json_decode(file_get_contents("php://input"), true);

		// Check if keys are set
		if( !empty($teamData['email'])
				&& !empty($teamData['captcha']) ){

			$email = trim($teamData['email']);

			if($email){
				// Check if team name contains unallowed chars
				if( filter_var($email, FILTER_VALIDATE_EMAIL) ){

					// Check if captcha was answered correctly
					$recaptcha_keys = array(
						'site_key' => get_cfg_var("bka.cfg.RECAPTCHA_SITE_KEY"),
						'secret_key' => get_cfg_var("bka.cfg.RECAPTCHA_SECRET_KEY")
					);

					$recaptcha = new Recaptcha($recaptcha_keys);

					if($recaptcha->verify($teamData['captcha'])){

						// Database Object
						$database = new Db();

						// Check if team exits and if it has specifies an email address
						$database->bind("email", $email);
						$team = $database->query("SELECT	id,
															name
														FROM teams
														WHERE email = :email
														LIMIT 1;");

						if(sizeof($team) == 1){

							$teamID = $team[0]["id"];
							$teamName = $team[0]["name"];

							// Check for last recovery
							$database->bind("cooldown", RECOVER_COOLDOWN_MINUTES);
							$database->bind("teamID", $teamID);
							$recoverInfo = $database->query("SELECT	id
																	FROM recover
																	WHERE created >= (now() - INTERVAL :cooldown MINUTE)
																		AND teamID = :teamID;");

							if(sizeof($recoverInfo) == 0){
								// Create recover entry
								$recoverToken = generateRandomString(RECOVER_TOKEN_LENGTH);

								$database->bind("teamID", $teamID);
								$database->bind("token", $recoverToken);
								$team = $database->query("INSERT INTO	recover
																	(
																		teamID,
																		token
																	)
																	VALUES
																	(
																		:teamID,
																		:token
																	);");

								// Send recover email
								$to			=	$email;
								$subject	=	'Passwort wiederherstellen für GC6AVF1';
								$message	=	"Hallo ".$teamName.",\r\n\r\nÜber folgenden Link könnt Ihr euer Passwort wiederherstellen:\r\n\r\nhttp://bka-gc.xyz/confirmRecover.php?t=".$recoverToken."\r\n\r\nSolltest du das Zurücksetzen des Passworts nicht angefordert haben, ignoriere diese Mail einfach!";
								$headers	=	'From: "BKA" <'.get_cfg_var("bka.cfg.OWNER_EMAIL").'>' . "\r\n" .
												'Reply-To: '.get_cfg_var("bka.cfg.OWNER_EMAIL")."\r\n" .
												'Content-Type: text/plain; charset=UTF-8' . "\r\n" .
												'X-Mailer: PHP/' . phpversion();

								mail($to, $subject, $message, $headers);

								$returnValue["status"] = "success";
							}else{
								$returnValue["status"] = "error";
								$returnValue["message"] = "Das Passwort darf nur alle " . RECOVER_COOLDOWN_MINUTES . " Minuten zurückgesetzt werden!";
							}
						}else{
							$returnValue["status"] = "error";
							$returnValue["message"] = "Diese Email-Adresse konnte nicht gefunden werden!";
						}
					}else{
						$returnValue["status"] = "error";
						$returnValue["message"] = "Sie konnten nicht als echter Mensch verifiziert werden!";
					}
				}else{
					$returnValue["status"] = "error";
					$returnValue["message"] = "Bitte geben Sie eine gültige Email-Adresse ein!";
				}
			}else{
				$returnValue["status"] = "error";
				$returnValue["message"] = "Bitt geben Sie etwas ein!";
			}

			// Answer
			header("Content-Type: application/json; charset=utf-8");
			echo json_encode($returnValue);
		}
	}
?>