<?php

	require_once(__DIR__."/../include/Db.class.php");
	require_once(__DIR__."/../include/constants.php");
	require_once(__DIR__."/../include/helper/session.php");

	session_start();

	session_restore();

	if($_SESSION["teamID"]){

		$returnValue = array();

		// Database Object
		$database = new Db();

		// Update team
		$database->bind("teamID", $_SESSION["teamID"]);
		$stmt = $database->query("UPDATE teams
										SET seenLast = now()
										WHERE id = :teamID;");

		// Get number of teams that are online
		$database->bind("seconds", TIME_DELTA_ONLINE);
		$teams = $database->query("SELECT	COUNT(*) AS onlineCount
										FROM teams
										WHERE seenLast >= DATE_SUB(NOW(),INTERVAL :seconds SECOND)
											AND loggedIn = 1;");
		$returnValue["online"] = $teams[0]["onlineCount"];

		// Get number of valid hashes
		$database->bind("status_valid", BLOCK_STATUS_VALID);
		$blocks = $database->query("SELECT	SUM(blocksAccepted) AS hashCount
										FROM teams;");
		$returnValue["blocks"] = $blocks[0]["hashCount"];

		// Get average time per block
		// Note: Consumes too much memory
		// Todo: replace by stored procedure :P
		/*
			$database->bind("status_valid", BLOCK_STATUS_VALID);
			$times = $database->query("SELECT	TIME_TO_SEC(TIMEDIFF(leafs.foundOn, startTime.time)) AS sinceStart
											FROM	leafs,
													(
														SELECT	leafs.foundOn AS time 
															FROM leafs, (SELECT MIN(id) AS id FROM leafs WHERE id > 1) minID
															WHERE leafs.id = minID.id
													) startTime
											WHERE leafs.status = :status_valid
												AND leafs.foundOn >= startTime.time;");

			$offset = 0;
			$timeSum = 0;
			$counter = 0;
			$mPerBlock = 0;
			
			foreach($times as $time){
				$timeSum += intval($time["sinceStart"]) - $offset;
				$offset = intval($time["sinceStart"]);
				$counter++;
			}

			if($counter){
				$mPerBlock = floor( (($timeSum / $counter) / 60) * 10) / 10;
			}

			$returnValue["tpb"] = $mPerBlock;
		*/

		$returnValue["tpb"] = 0;

		// Answer
		header("Content-Type: application/json; charset=utf-8");
		echo json_encode($returnValue);
	}
?>