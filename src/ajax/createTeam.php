<?php

	require_once(__DIR__."/../include/helper/validation.php");
	require_once(__DIR__."/../include/helper/rand.php");
	require_once(__DIR__."/../include/Db.class.php");
	require_once(__DIR__."/../include/constants.php");
	require_once(__DIR__."/../include/libs/random-name-generator/Generator.php");
	require_once(__DIR__."/../include/libs/random-name-generator/AbstractGenerator.php");
	require_once(__DIR__."/../include/libs/random-name-generator/Alliteration.php");
	require_once(__DIR__."/../include/libs/recaptcha/recaptcha.php");

	session_start();

	// Check if data was sent and if it is JSON
	if( isXMLHTTPRequest()
			&& !empty($_POST) 
				&& isJSON(file_get_contents("php://input")) ){

		$returnValue = array();
		$teamData = json_decode(file_get_contents("php://input"), true);

		// Check if keys are set
		if( isset($teamData['name'])
				&& !empty($teamData['privacy_policy'])
					&& !empty($teamData['briefing'])
						&& isset($teamData['email'])
							&& !empty($teamData['captcha']) ){

			if($teamData['privacy_policy'] == true && $teamData['briefing'] == true){

				$teamName = trim($teamData['name']);
				$email = trim($teamData['email']);

				if( !$email || filter_var($email, FILTER_VALIDATE_EMAIL) ){
					if(strlen($teamName) == 0 || (strlen($teamName) >= TEAM_MIN_NAME_LENGHT && strlen($teamName) <= TEAM_MAX_NAME_LENGHT) ){

						// Check if team name contains unallowed chars
						if(!preg_match('/[^\x20-\x7e]/', $teamName)){

							// Check if captcha was answered correctly
							$recaptcha_keys = array(
								'site_key' => get_cfg_var("bka.cfg.RECAPTCHA_SITE_KEY"),
								'secret_key' => get_cfg_var("bka.cfg.RECAPTCHA_SECRET_KEY")
							);

							$recaptcha = new Recaptcha($recaptcha_keys);

							if($recaptcha->verify($teamData['captcha'])){

								// Database Object
								$database = new Db();

								// Check if we have reached the maximum number of teams
								// Anonymous teams don't count
								$teams = $database->query("SELECT COUNT(*) AS teamCount FROM teams WHERE deleted = 0 AND isAnonymous = 0;");

								if(intval($teams[0]["teamCount"]) <= TEAM_MAX_ALLOWED_TEAMS){

									if(strlen($teamName) == 0){
										$teamFound = false;
										$generator = new \Nubs\RandomNameGenerator\Alliteration();

										// There should be enough data to generate a random name,
										// else the standard error message is thrown in the following check
										for($i = 0; $i < 100; $i++){
											// Generate random team name
											$teamName = $generator->getName();
											if( strlen($teamName) <= TEAM_MAX_NAME_LENGHT ){
												$database->bind("name", $teamName);
												$teams = $database->query("SELECT id FROM teams
																				WHERE name = :name;");
												if(sizeof($teams) == 0){
													$teamFound = true;
													break;
												}
											}
										}
									}

									// Check if Team already exists
									if($email){
										$database->bind("name", $teamName);
										$database->bind("email", $email);
										$teams = $database->query("SELECT id FROM teams
																		WHERE name = :name
																			OR email = :email;");
									}else{
										$database->bind("name", $teamName);
										$teams = $database->query("SELECT id FROM teams
																		WHERE name = :name;");
									}

									// Check if teams exists already
									if(sizeof($teams) == 0){

										// Check if teamname is reserved
										$database->bind("name", $teamName);
										$teams = $database->query("SELECT id FROM reserved_teamnames
																		WHERE name = :name;");

										if(sizeof($teams) == 0){
											// Create password
											$password = generateRandomString(TEAM_PASSWORD_LENGHT);

											// Hash password
											$passwordHashed = password_hash($password, PASSWORD_DEFAULT);

											// Insert into database
											$database->bind("name", $teamName);
											$database->bind("password", $passwordHashed);
											$database->bind("email", $email);
											$database->query("INSERT INTO teams
																				(
																					name,
																					password,
																					email,
																					created
																				)
																				VALUES
																				(
																					:name,
																					:password,
																					:email,
																					NOW()
																				);");

											// Send mail with password
											if($email){
												$to			=	$email;
												$subject	=	'Anmeldedaten für GC6AVF1';
												$message	=	"Hallo ".$teamName.",\r\n\r\nEuer Teamname lautet : ".$teamName."\r\nEuer Passwort lautet : ".$password."\r\n\r\nViel Spaß!";
												$headers	=	'From: "BKA" <'.get_cfg_var("bka.cfg.OWNER_EMAIL").'>' . "\r\n" .
																'Reply-To: '.get_cfg_var("bka.cfg.OWNER_EMAIL")."\r\n" .
																'Content-Type: text/plain; charset=UTF-8' . "\r\n" .
																'X-Mailer: PHP/' . phpversion();

												mail($to, $subject, $message, $headers);
											}

											$returnValue["status"] = "success";
											$returnValue["password"] = $password;
											$returnValue["team"] = $teamName;
										}else{
											$returnValue["status"] = "error";
											$returnValue["message"] = "Dies ist ein reservierter Name, bitte benutzen Sie einen anderen!";
										}
									}else{
										$returnValue["status"] = "error";
										$returnValue["message"] = "Ein Team mit diesem Namen, oder der Email-Adresse existiert bereits!";
									}
								}else{
									$returnValue["status"] = "error";
									$returnValue["message"] = "Es wurde die maximale Anzahl an angemeldeten Teams erreich, bitte wenden Sie sich an den Owner!";
								}
							}else{
								$returnValue["status"] = "error";
								$returnValue["message"] = "Sie konnten nicht als echter Mensch verifiziert werden!";
							}
						}else{
							$returnValue["status"] = "error";
							$returnValue["message"] = "Der Teamname darf nur aus ASCII-Zeichen bestehen!";
						}
					}else{
						$returnValue["status"] = "error";
						$returnValue["message"] = "Der Teamname muss zwischen ".TEAM_MIN_NAME_LENGHT." und ".TEAM_MAX_NAME_LENGHT." Zeichen lang sein!";
					}
				}else{
					$returnValue["status"] = "error";
					$returnValue["message"] = "Bitte geben Sie eine korrekte Email ein!";
				}
			}else{
				$returnValue["status"] = "error";
				$returnValue["message"] = "Sie müssen der Datenzschutzerklärung zustimmen und das Briefung lesen!";
			}

			// Answer
			header("Content-Type: application/json; charset=utf-8");
			echo json_encode($returnValue);
		}
	}
?>