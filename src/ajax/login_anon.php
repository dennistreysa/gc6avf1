<?php
	
	require_once(__DIR__."/../include/helper/validation.php");
	require_once(__DIR__."/../include/helper/rand.php");
	require_once(__DIR__."/../include/helper/session.php");
	require_once(__DIR__."/../include/Db.class.php");
	require_once(__DIR__."/../include/constants.php");
	require_once(__DIR__."/../include/libs/recaptcha/recaptcha.php");

	if( isXMLHTTPRequest()
		&& !empty($_POST) 
			&& isJSON(file_get_contents("php://input")) ){

		$returnValue = array();
		$loginData = json_decode(file_get_contents("php://input"), true);

		// Check if keys are set
		if( !empty($loginData['captcha']) ){

			if( !empty($loginData['privacy_policy'])
				&& $loginData['privacy_policy'] == true 
				&& !empty($loginData['briefing'])
				&& $loginData['briefing'] == true ){

				// Check if captcha was answered correctly
				$recaptcha_keys = array(
					'site_key' => get_cfg_var("bka.cfg.RECAPTCHA_SITE_KEY"),
					'secret_key' => get_cfg_var("bka.cfg.RECAPTCHA_SECRET_KEY")
				);

				$recaptcha = new Recaptcha($recaptcha_keys);

				if($recaptcha->verify($loginData['captcha'])){

					$database = new Db();

					// Get number of currently active sessions
					$sessions = $database->query("SELECT COUNT(*) AS count FROM anonymous_sessions;");

					if(intval($sessions[0]["count"]) <= ANONYMOUS_MAX_ALLOWED_SESSIONS){

						$sessionCookie = "";

						while(true){
							// Generate random session
							$sessionCookie = generateRandomString(ANONYMOUS_SESSION_STR_LEN);
							
							$database->bind("cookie", $sessionCookie);
							$teams = $database->query("SELECT id FROM anonymous_sessions
															WHERE cookie = :cookie;");
							if(sizeof($teams) == 0){
								break;
							}
						}

						$database->bind("cookie", $sessionCookie);
						$database->query("INSERT INTO anonymous_sessions (cookie) VALUES (:cookie);");

						$cookie = $sessionCookie.'-'.md5($sessionCookie.get_cfg_var("bka.cfg.ANON_COOKIE_SALT"));

						setcookie("SESSION_PERSISTENT", $cookie, time()+365*24*60*60, "/");

						$returnValue["status"] = "success";
					}else{
						$returnValue["status"] = "error";
						$returnValue["message"] = "Die Anzahl der maximal erlaubten anonymen Sessions wurde überschritten, bitte kontaktiere den Owner!";
					}
				}else{
					$returnValue["status"] = "error";
					$returnValue["message"] = "Sie konnten nicht als echter Mensch verifiziert werden!";
				}
			}else{
				$returnValue["status"] = "error";
				$returnValue["message"] = "Sie müssen der Datenzschutzerklärung zustimmen und das Briefung lesen!";
			}
			
		}else{
			$returnValue["status"] = "error";
			$returnValue["message"] = "Bitte das Captcha beantworten!";
		}

		// Answer
		header("Content-Type: application/json; charset=utf-8");
		echo json_encode($returnValue);
	}
?>