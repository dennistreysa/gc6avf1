<?php

	require_once(__DIR__."/include/helper/session.php");

	session_start();

	session_restore(true);

	if( isset($_SESSION["teamID"]) ){
?>

<!DOCTYPE html>
<html lang="de">
	<head>
		<title>GC6AVF1 | Übersicht</title>

		<link rel="icon" href="img/favicon.png">

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- External includes -->
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css">
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
			<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css">
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.10/css/jquery.dataTables.min.css">
			
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.10/js/jquery.dataTables.js"></script>

		<!-- Local includes -->
			<script src="js/teams.js"></script>
			<script src="js/stats.js"></script>
			
	</head>

	<body style="padding-top: 80px;">

		<!-- Fixed navbar -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">GC6AVF1</a>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="active"><a><i class="fa fa-bar-chart"></i>&nbsp;Übersicht</a></li>
						<li><a href="decrypt.php"><i class="fa fa-bolt"></i>&nbsp;Entschlüsseln</a></li>
						<li><a href="message.php"><i class="fa fa-unlock"></i>&nbsp;Entschlüsselte Nachricht</a></li>
						<li><a href="briefing.html" target="_blank"><i class="fa fa-info"></i>&nbsp;Info</a></li>
					</ul>

					<ul class="nav navbar-nav navbar-right">
						<li><a><i class="fa fa-users"></i>&nbsp;Teams online : <span id="teams_online_counter">0</span></a></li>
						<li><a><i class="fa fa-unlock"></i>&nbsp;Entschlüsselte Blöcke (global) : <span id="decrypted_blocks_counter">0</span></a></li>
						<!--<li><a><i class="fa fa-clock-o"></i>&nbsp;Min/Block : <span id="time_per_block">0</span></a></li>-->
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION["name"]; ?> <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="fa fa-sign-out"></i>Ausloggen</a></li>
							</ul>
						</li>
					</ul>

				</div><!--/.nav-collapse -->

			</div>
		</nav>

		<div id="main-container" class="container-fluid text-center">

			<div class="row">
				<div class="text-center">
					<div class="col-md-10 col-md-offset-1">
						<h2>Team-Liste:</h2>
					</div>
				</div>
			</div>	

			<div class="row">
				<div class="text-center">
					<div class="col-md-10 col-md-offset-1">
						<table id="table_teams" class="display" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Name</th>
									<th>Eingesendete Blöcke</th>
									<th>Akzeptierte Blöcke</th>
									<th>% entschlüsselt</th>
									<th>Maximale Kosten</th>
									<th>Summe aller Kosten</th>
									<th>Beigetreten</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Name</th>
									<th>Eingesendete Blöcke</th>
									<th>Akzeptierte Blöcke</th>
									<th>% entschlüsselt</th>
									<th>Maximale Kosten</th>
									<th>Summe aller Kosten</th>
									<th>Beigetreten</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>	

			<div class="row">
				<div class="text-center">
					<div class="col-md-10 col-md-offset-1">
						<h6>Nur Teams mit mindestes einem eingesendeten Block werden aufgelistet.</h6>
					</div>
				</div>
			</div>

		</div>

	</body>
</html>

<?php
	}else{
		header("Location: login.php");
	}
?>