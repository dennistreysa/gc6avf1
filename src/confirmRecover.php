<?php

	require_once(__DIR__."/include/helper/validation.php");
	require_once(__DIR__."/include/helper/rand.php");
	require_once(__DIR__."/include/Db.class.php");
	require_once(__DIR__."/include/constants.php");

	usleep(500000);

	if( !empty($_GET['t'])
			&& is_string($_GET['t'])
			&& strlen($_GET['t']) == RECOVER_TOKEN_LENGTH ){

		$database = new Db();

		// Check if that token exists
		$database->bind("token", $_GET['t']);
		$database->bind("cooldown", RECOVER_COOLDOWN_MINUTES);
		$recoverInfo = $database->query("SELECT	recover.id AS id,
												recover.teamID AS teamID,
												teams.email AS email,
												teams.name AS name
												FROM recover, teams
												WHERE recover.token = :token
													AND recover.created >= (now() - INTERVAL :cooldown MINUTE)
													AND recover.used = 0
													AND recover.teamID = teams.id
												LIMIT 1;");

		if(sizeof($recoverInfo) == 1){

			$recoverID = $recoverInfo[0]["id"];
			$teamID = $recoverInfo[0]["teamID"];
			$email = $recoverInfo[0]["email"];
			$teamName = $recoverInfo[0]["name"];
			
			// Create new password
			$password = generateRandomString(TEAM_PASSWORD_LENGHT);

			// Hash password
			$passwordHashed = password_hash($password, PASSWORD_DEFAULT);

			// Insert into database
			$database->bind("password", $passwordHashed);
			$database->bind("teamID", $teamID);
			$database->query("UPDATE teams
									SET password = :password
									WHERE id = :teamID;");

			// Update recover info
			$database->bind("recoverID", $recoverID);
			$database->query("UPDATE recover
									SET used = 1
									WHERE id = :recoverID;");

			$to			=	$email;
			$subject	=	'Neues Passwort für GC6AVF1';
			$message	=	"Hallo ".$teamName.",\r\n\r\nEuer neues Passwort lautet: ".$password;
			$headers	=	'From: "BKA" <dennistreysa@gmail.com>' . "\r\n" .
							'Reply-To: dennistreysa@gmail.com' . "\r\n" .
							'Content-Type: text/plain; charset=UTF-8' . "\r\n" .
							'X-Mailer: PHP/' . phpversion();

			mail($to, $subject, $message, $headers);

			echo "Passwort erfolgreich zurückgesetzt, eine Email mit dem neuen Passwort wurde versendet!";
		}else{
			echo "Dieses Token wurde bereits benutzt, oder ist unbekannt!";
		}
	}
?>