<?php

	require_once(__DIR__."/include/helper/session.php");

	session_start();

	session_restore(true);

	if( isset($_SESSION["teamID"]) ){
?>

<!DOCTYPE html>
<html lang="de">
	<head>
		<title>GC6AVF1 | Entschlüsseln</title>

		<link rel="icon" href="img/favicon.png">

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- External includes -->
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css">
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
			<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css">
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.10/css/jquery.dataTables.min.css">
			
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.10/js/jquery.dataTables.js"></script>
			<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
			<script type="text/javascript" src="js/asmcrypto.js"></script>

		<!-- Local includes -->
			<script src="js/decrypt.js"></script>
			<script src="js/stats.js"></script>
			
	</head>

	<body style="padding-top: 80px;">

		<!-- Fixed navbar -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">GC6AVF1</a>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a href="index.php"><i class="fa fa-bar-chart"></i>&nbsp;Übersicht</a></li>
						<li class="active"><a><i class="fa fa-bolt"></i>&nbsp;Entschlüsseln</a></li>
						<li><a href="message.php"><i class="fa fa-unlock"></i>&nbsp;Entschlüsselte Nachricht</a></li>
						<li><a href="briefing.html" target="_blank"><i class="fa fa-info"></i>&nbsp;Info</a></li>
					</ul>

					<ul class="nav navbar-nav navbar-right">
						<li><a><i class="fa fa-users"></i>&nbsp;Teams online : <span id="teams_online_counter">0</span></a></li>
						<li><a><i class="fa fa-unlock"></i>&nbsp;Entschlüsselte Blöcke (global) : <span id="decrypted_blocks_counter">0</span></a></li>
						<!--<li><a><i class="fa fa-clock-o"></i>&nbsp;Min/Block : <span id="time_per_block">0</span></a></li>-->
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION["name"]; ?> <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="fa fa-sign-out"></i>Ausloggen</a></li>
							</ul>
						</li>
					</ul>

				</div><!--/.nav-collapse -->

			</div>
		</nav>

		<div id="main-container" class="container-fluid text-center">
			
			<div class="row">
				<div class="text-center">
					<div class="col-md-4 col-md-offset-4">
						<button id="btn-decrypt" type="button" style="margin-top: 30px;" class="btn btn-success" onclick="Decrypt();">Starte Entschlüsselung</button>
						<div style="display:none;" id="working-spinner"><i class="fa fa-spin fa-cog"></i>&nbsp;Knacke Passwörter...</div>
					</div>
				</div>
			</div>

			<div class="row" style="margin-top: 50px;">
				<div class="col-md-6 col-md-offset-1">
					<div class="text-center">
						<div id="speed_graph"></div>
					</div>
				</div>
				<div class="col-md-5">
					<div class="text-left">
						<div>Aktuelles Target : <span id="current_target"></span></div>
						<div>Aktueller Knoten : <span id="current_leaf"></span></div>
						<div>Korrekte Passwörter gefunden (diese Session) : <span id="valid_hashes"></span></div>
						<div>Passwörter pro Sekunde : <span id="hashes_per_second"></span></div>
					</div>
				</div>
			</div>

			<div class="row" style="margin-top: 50px;">
				<div class="text-center">
					<div class="col-md-10 col-md-offset-1">
						<h3>Block-Übersicht</h3>
					</div>
				</div>
			</div>

			<div class="row" style="margin-top: 20px;">
				<div class="text-center">
					<div class="col-md-10 col-md-offset-1">
						<table id="table_decrypt" class="display" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Block Nr.</th>
									<th>Datum</th>
									<th>Kosten</th>
									<th>Status</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Block Nr.</th>
									<th>Datum</th>
									<th>Kosten</th>
									<th>Status</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>	
		</div>

	</body>
</html>

<?php
	}else{
		header("Location: login.php");
	}
?>