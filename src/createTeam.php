<?php

	require_once(__DIR__."/include/helper/session.php");
	
	session_start();

	session_restore();

	if( !isset($_SESSION["teamID"]) ){
?>

<!DOCTYPE html>
<html lang="de">
	<head>
		<title>GC6AVF1 | Team erstellen</title>

		<link rel="icon" href="img/favicon.png">

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- External includes -->
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
			<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css">
			
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
			<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
			<script src='https://www.google.com/recaptcha/api.js'></script>

		<!-- Local includes -->
			<link rel="stylesheet" href="css/login.css">
			
			<script src="js/login.js"></script>
			<script src="js/messages_de.js"></script>
	</head>

	<body>
		<div class="row">
			<div class="text-center" style="font-size:xx-small;">
				Dies ist eine private, nichtkommerzielle Webseite, die für einen <a href="https://de.wikipedia.org/wiki/Geocaching">Rätsel-Geocache</a> erstellt wurde! Dies ist keine offizielle Webseite des BKA!
			</div>
		</div>
		<div class="row">
			<div class="text-center">
				<img src="img/bka.png">
			</div>
		</div>
		<div class="row">
			<div class="text-center">
				<div class="logo">Registrierung</div>
				<div class="login-form-1">
					<form id="register-form" class="text-left">
						<div class="etc-login-form">
							<p>Sie möchten uns also unterstützen? Bevor es losgeht müssen Sie erst an der <a href="briefing.html">Einsatzbesprechung</a> teilnehmen!</p>
							<div class="text-center">
								<a href="anonym.php" class="btn btn-info" role="button">Völlig anonym mitmachen?</a>
							</div>
						</div>
						<div class="login-form-main-message"></div>
						<div class="main-login-form">
							<div class="login-group">
								<div class="form-group">
									<label for="reg_team_name" class="sr-only">Teamname (freiwillig)</label>
									<input type="text" class="form-control" id="reg_team_name" name="reg_team_name" placeholder="Teamname (freiwillig)" pattern="[a-zA-Z0-9!@#$%^*_|]{6,25}">
								</div>
								<div class="form-group">
									<label for="reg_email" class="sr-only">Email (freiwillig)</label>
									<input type="text" class="form-control" id="reg_email" name="reg_email" placeholder="Email (freiwillig)">
								</div>
								<div class="form-group reg_team-checkbox">
									<input type="checkbox" id="briefing" name="briefing">
									<label for="briefing">Ich habe an der <a href="briefing.html">Teambesprechung</a> teilgenommen</label>
								</div>
								<div class="form-group reg_team-checkbox">
									<input type="checkbox" id="privacy_policy" name="privacy_policy">
									<label for="privacy_policy">Ich stimme der <a href="briefing.html#dse" target="_blank">Datenschutzerklärung</a> zu und habe den <a href="briefing.html#ha" target="_blank">Haftungsausschluss</a> gelesen und verstanden!</label>
								</div>
								<div class="form-group text-center">
									<div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="6Lc4ZhUTAAAAAKYpxMcttNXFEqW3r5v3_wc6hRwF"></div>
								</div>
							</div>
							<button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
						</div>
						<div class="etc-login-form">
							<p>Sie haben schon ein Team? <a href="login.php">Zum Login</a></p>
							<p>Zur <a href="briefing.html#dse" target="_blank">Datenschutzerklärung</a></p>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>

<?php
	}else{
		header("Location: index.php");
	}
?>