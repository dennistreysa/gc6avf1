
$( document ).ready(function() {
	$('#table_teams').DataTable({
		"ajax": "ajax/getTeamsTable.php",
		"columns": [
					{ "data": "name", "type": "string" },
					{ "data": "leafCount", "type": "num" },
					{ "data": "validLeafCount", "type": "num" },
					{ "data": "percent", "type": "num" },
					{ "data": "maxCost", "type": "num" },
					{ "data": "sumCost", "type": "num" },
					{ "data": "created", "type": "date" }
		],
		"order": [[ 2, "desc" ]],
		"pageLength": 50
	});
});