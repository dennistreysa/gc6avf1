
var isCreatingTeam = false;
var isLoggingIn = false;
var isRecovering = false;
var captchaChecked = false;

$( document ).ready(function() {

	/*
	 *	Login
	 */
		$("#login-form").validate({
			rules: {
				lg_teamname: "required",
				lg_password: "required"
			},
			errorClass: "form-invalid"
		});

		$("#login-form").submit(function(event) {

			event.preventDefault();

			var $form = $(this);

			remove_loading($form);

			// Check if form-data is valid
			if($form.valid()){

				var teamName = $("#lg_teamname").val().trim();
				var password = $("#lg_password").val().trim();

				if(!isLoggingIn){
					if(teamName){
						if(grecaptcha.getResponse().length){

							form_loading($form, "Logge ein, bitte warten.");

							isLoggingIn = true;

							var teamData = {
											name : teamName,
											password : password,
											captcha : $("#g-recaptcha-response").val()
										};

							$.ajax({
									type: 'POST',
									url: "ajax/login.php",
									data: JSON.stringify(teamData),
									dataType: 'json'
								}).done(function( data ) {

									isLoggingIn = false;

									if(data){

										if(data.status === "success"){
											form_success($form, "Sie wurden erfolgreich eingeloggt!");
											setTimeout(function(){
												window.location.replace("index.php");
											}, 2000);

										}else{
											isLoggingIn = false;
											form_failed($form, data.message);
										}
									}else{
										//console.log(data);
										form_failed($form, "Beim Login ist etwas schiefgelaufen!");
									}
								}).fail( function( data ) {
									//console.log(data);
									isLoggingIn = false;
									form_failed($form, "Beim Login ist etwas schiefgelaufen!");
								});
						}else{
							form_failed($form, "Bitte bestätigen Sie, dass Sie ein Mensch sind!");
						}
					}else{
						setTimeout(function(){
							form_failed($form, "Bitte geben Sie einen Namen ein!");
						}, 2000);
					}
				}
			}
		});

		$("#login_anon-form").validate();

		$("#login_anon-form").submit(function(event) {

			event.preventDefault();

			var $form = $(this);

			remove_loading($form);

			// Check if form-data is valid
			if($form.valid()){

				if(!isLoggingIn){
					if(grecaptcha.getResponse().length){
						if($('#briefing').is(":checked") && $('#privacy_policy').is(":checked")){

							form_loading($form, "Logge ein, bitte warten.");

							isLoggingIn = true;

							var teamData = {
											captcha : $("#g-recaptcha-response").val(),
											privacy_policy : $('#privacy_policy').is(":checked"),
											briefing : $('#briefing').is(":checked")
										};

							$.ajax({
									type: 'POST',
									url: "ajax/login_anon.php",
									data: JSON.stringify(teamData),
									dataType: 'json'
								}).done(function( data ) {

									isLoggingIn = false;

									if(data){

										if(data.status === "success"){
											form_success($form, "Sie wurden erfolgreich eingeloggt!");
											setTimeout(function(){
												window.location.replace("index.php");
											}, 2000);

										}else{
											console.log(data);
											isLoggingIn = false;
											form_failed($form, data.message);
										}
									}else{
										console.log(data);
										form_failed($form, "Beim Login ist etwas schiefgelaufen!");
									}
								}).fail( function( data ) {
									console.log(data);
									isLoggingIn = false;
									form_failed($form, "Beim Login ist etwas schiefgelaufen!");
								});
						}else{
							setTimeout(function(){
								form_failed($form, "Sie müssen der Datenschutzerklärung zustimmen und am Briefing teilnehmen!");
							}, 1000);
						}
					}else{
						form_failed($form, "Bitte bestätigen Sie, dass Sie ein Mensch sind!");
					}
				}
			}
		});

	/*
	 *	Registration
	 */
		$("#register-form").validate({
			rules: {
				reg_team_name: {
					maxlength : 20
				},
				reg_email: {
					email : true,
					required : false
				}
			},
			errorClass: "form-invalid"
		});

		$("#register-form").submit(function(event) {

			event.preventDefault();

			var $form = $(this);

			remove_loading($form);

			// Check if form-data is valid
			if($form.valid()){
				if(grecaptcha.getResponse().length){
					form_loading($form, "Bitte haben Sie einen Moment Gedult, Ihr Konto wird für Sie angelegt.");

					var teamName = $("#reg_team_name").val().trim();

					if(!isCreatingTeam){
						if( teamName.length === 0 || (teamName.length >= 3 && teamName.length <= 20) ){
							if(!teamName.match(/[^\x20-\x7e]/)){
								if($('#briefing').is(":checked") && $('#privacy_policy').is(":checked")){

									isCreatingTeam = true;

									var teamData = {
												name : teamName,
												email : $("#reg_email").val().trim(),
												privacy_policy : $('#privacy_policy').is(":checked"),
												briefing : $('#briefing').is(":checked"),
												captcha : $("#g-recaptcha-response").val()
											};

									$.ajax({
										type: 'POST',
										url: "ajax/createTeam.php",
										data: JSON.stringify(teamData),
										dataType: 'json'
									}).done(function( data ) {

										isCreatingTeam = false;

										if(data){

											if(data.status === "success"){
												form_success($form, "Ihr Team wurde erfolgreich angelegt!<br>Teamname : "+data.team+"<br>Passwort : "+data.password);
											}else{
												isCreatingTeam = false;
												form_failed($form, data.message);
											}
										}else{
											//console.log(data);
											form_failed($form, "Beim Erstellen des Teams ist etwas schiefgelaufen!");
										}
									}).fail( function( data ) {
										//console.log(data);
										isCreatingTeam = false;
										form_failed($form, "Beim Erstellen des Teams ist etwas schiefgelaufen!");
									});

								}else{
									setTimeout(function(){
										form_failed($form, "Sie müssen der Datenschutzerklärung zustimmen und am Briefing teilnehmen!");
									}, 1000);
								}
							}else{
								setTimeout(function(){
									form_failed($form, "Der Teamname darf nur ASCII-Zeichen enthalten!");
								}, 1000);
							}
						}else{
							setTimeout(function(){
								form_failed($form, "Der Teamname muss zwischen 3 und 20 Zeichen lang sein!");
							}, 1000);
						}
					}
				}else{
					form_failed($form, "Bitte bestätigen Sie, dass Sie ein Mensch sind!");
				}
			}
		});

	/*
	 *	Recover password
	 */
		$("#recover-form").validate({
				rules: {
					email: "required"
				},
				errorClass: "form-invalid"
		});

		$("#recover-form").submit(function(event) {

			console.log("test");

			event.preventDefault();

			var $form = $(this);

			remove_loading($form);

			// Check if form-data is valid
			if($form.valid()){

				var email = $("#email").val().trim();

				if(!isRecovering){
					if(email){
						if(grecaptcha.getResponse().length){

							form_loading($form, "Sende Daten, bitte warten.");

							isRecovering = true;

							var teamData = {
											email : email,
											captcha : $("#g-recaptcha-response").val()
										};

							$.ajax({
									type: 'POST',
									url: "ajax/recover.php",
									data: JSON.stringify(teamData),
									dataType: 'json'
								}).done(function( data ) {

									isRecovering = false;

									if(data){

										if(data.status === "success"){
											form_success($form, "Ein neues Passwort wurde an Ihre Emailadresse gesendet!");
											setTimeout(function(){
												window.location.replace("index.php");
											}, 3000);

										}else{
											isRecovering = false;
											form_failed($form, data.message);
										}
									}else{
										form_failed($form, "Beim Wiederherstellen des Passworts ist etwas schiefgelaufen!");
									}
								}).fail( function( data ) {
									isRecovering = false;
									form_failed($form, "Beim Wiederherstellen des Passworts ist etwas schiefgelaufen!");
								});
						}else{
							form_failed($form, "Bitte bestätigen Sie, dass Sie ein Mensch sind!");
						}
					}else{
						setTimeout(function(){
							form_failed($form, "Bitte geben Sie Ihre Email ein!");
						}, 2000);
					}
				}
			}
		});

});

function remove_loading($form){
	$form.find('[type=submit]').removeClass('error success');
	$form.find('.login-form-main-message').removeClass('animated shake bounce show error success').html('');
}

function form_loading($form, message){
	$form.find('[type=submit]').addClass('clicked').html('<i class="fa fa-spinner fa-pulse"></i>');
	if(message){
		$form.find('.login-form-main-message').addClass('show info').html(message);
	}
}

function form_success($form, message){
	$form.find('.login-form-main-message').removeClass('animated shake bounce show info');
	$form.find('[type=submit]').addClass('success').html('<i class="fa fa-check"></i>');
	$form.find('.login-form-main-message').addClass('animated bounce show success').html(message);
}

function form_failed($form, message){
	$form.find('.login-form-main-message').removeClass('animated shake bounce show info');
	$form.find('[type=submit]').addClass('error').html('<i class="fa fa-remove"></i>');
	$form.find('.login-form-main-message').addClass('animated shake show error').html(message);
}