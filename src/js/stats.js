
$( document ).ready(function() {
	updateStats();
});

function updateStats(){
	$.ajax({
			type: 'GET',
			url: "ajax/getStats.php",
			dataType: 'json'
		}).done(function( data ) {

			if(data){
				$("#teams_online_counter").html(data.online);
				$("#decrypted_blocks_counter").html(data.blocks);
				$("#time_per_block").html(data.tpb);

				setTimeout(function(){ updateStats(); }, (5*60*1000));
			}
		});
}