
var g_targetBytes = [];
var g_targetStr = "";
var g_leaf = "";
var g_speedData = [["Uhrzeit", "p/s"]];
var g_isDecrypting = false;
var g_waitingForData = true;
var g_hashesTotal = 0;
var g_timePassed = 0;
var g_GUIUpdateTimer = 0;
var g_DataUpdateTimer = 0;
var g_BlockUpdateTimer = 0;
var g_GraphUpdateCounter = 0;
var g_validHashes = 0;
var g_teamName = "";
var g_blockTable = null;

$( document ).ready(function() {
	
	// Blockchain table
		updateBlockTable();

	// Speed Chart stuff
		google.charts.load('current', {'packages':['corechart']});
		google.charts.setOnLoadCallback(drawChart);

		function drawChart() {
			var d = new Date();
			g_speedData.push([d,  0]);
			updateChart();
		}
});

// Updates the table that shows the blocks of the team
function updateBlockTable(){
	g_blockTable = $('#table_decrypt').DataTable({
						"ajax": {
							"url" : "ajax/getTeamBlocks.php",
							"dataSrc": function ( json ) {

								for ( var i = 0, len = json.length; i < len ; i++ ) {
									switch(parseInt(json[i].status)){
										case 1:{
											json[i].status = "1 : Akzeptiert";
											break;
										}
										case 2:{
											json[i].status = "2 : Ausstehend";
											break;
										}
										case 3:{
											json[i].status = "3 : Target veraltet";
											break;
										}
										case 4:{
											json[i].status = "4 : Knoten veraltet";
											break;
										}
										case 5:{
											json[i].status = "5 : Pfad verworfen";
											break;
										}
										default:{
											json[i].status = "FEHLER";
											break;
										}
									}
								}
								return json;
							}
						},
						"columns": [
									{ "data": "number", "type": "num" },
									{ "data": "found", "type": "date" },
									{ "data": "cost", "type": "num" },
									{ "data": "status", "type": "string" }
						],
						"order": [[ 0, "desc" ]],
						"pageLength": 50
					});
}


// function to update speed chart
function updateChart(){

	if(g_speedData.length > 100){
		g_speedData = g_speedData.slice(2, 100);
		g_speedData.unshift(["Uhrzeit", "p/s"]);
	}

	var chartData = google.visualization.arrayToDataTable(g_speedData);

	var options = {
		title: 'Passwörter pro Sekunde',
		hAxis: {	
					minValue: new Date(),
					title: 'Uhrzeit',
					titleTextStyle: {color: '#333'},
					format: 'HH:mm'
				},
		vAxis: {
					viewWindowMode:'explicit',
					viewWindow:{
						min: 0
					}
				},
		legend: {position: 'none'}
	};

	var chart = new google.visualization.AreaChart(document.getElementById('speed_graph'));

	chart.draw(chartData, options);
}


// Decrypt button
function Decrypt(){

	if(g_isDecrypting){

		bootbox.dialog({
			
			message: "Sind Sie ganz sicher, dass sie den Vorgang abbrechen möchten?",
			title: "Achtung!",
			buttons: {
				success: {
					label: "Ja",
					className: "btn-success",
					callback: stopDecryption
				},

				danger: {
					label: "Nein",
					className: "btn-danger",
					callback: function() {
						g_isDecrypting = true;
					}
				}
			}
		});
	}else{

		bootbox.dialog({
			
			message: "Sind Sie ganz sicher, dass sie den Vorgang starten möchten?<br>Bitte sorgen Sie für ausreichende Kühlung ihrer CPU!<br>Der Owner kann nicht für etwaige Schäden haftbar gemacht werden!",
			title: "Warnung!",
			buttons: {
				success: {
					label: "Ja",
					className: "btn-success",
					callback: startDecryption
				},

				danger: {
					label: "Nein",
					className: "btn-danger",
					callback: function() {}
				}
			}
		});
	}
}


function startDecryption(){

	var $button = $("#btn-decrypt");
	var $spinner = $("#working-spinner");

	g_isDecrypting = true;
	$spinner.show();
	$button.removeClass("btn-success").addClass("btn-danger").html('Vorgang abbrechen');

	setTimeout(function(){ decryptLoop(); }, 1);
}


function stopDecryption(){

	var $button = $("#btn-decrypt");
	var $spinner = $("#working-spinner");

	g_isDecrypting = false;
	g_DataUpdateTimer = 0;
	g_BlockUpdateTimer = new Date().getTime() / 1000;
	$spinner.hide();
	$button.removeClass("btn-danger").addClass("btn-success").html("Starte Entschlüsselung");
}


function finishedDecryption(){

	var $button = $("#btn-decrypt");

	$button.removeClass("btn-danger").addClass("btn-info").html("Entschlüsselung abgeschlossen!");
	$button.prop("disabled", true);
}


function decryptLoop(){

	if(g_isDecrypting){
		var d = new Date();

		var currentTime = d.getTime() / 1000;

		if(currentTime - g_GUIUpdateTimer > 30 ){

			// Update texts

			var hashesPerSecond = g_hashesTotal / (currentTime - g_GUIUpdateTimer);

			$("#hashes_per_second").html(hashesPerSecond);
			$("#valid_hashes").html(g_validHashes.toString());

			g_GUIUpdateTimer = currentTime;
			g_hashesTotal = 0;
			g_GraphUpdateCounter++;

			if(g_GraphUpdateCounter == 2){

				// Update graph

				g_GraphUpdateCounter = 0;
				g_speedData.push([d, hashesPerSecond]);

				updateChart();
			}
		}

		if( currentTime - g_DataUpdateTimer > (1 * 60) ){

			// Update data

			g_waitingForData = true;

			g_DataUpdateTimer = currentTime;

			$.ajax({
				type: 'GET',
				url: "ajax/getCurrentData.php",
				dataType: 'json'
			}).done(function( data ) {

				if(data){
					if(data.target && data.leaf && data.teamName){

						$("#current_target").html(data.target);
						$("#current_leaf").html(data.leaf);

						g_targetStr = data.target;
						g_targetBytes = hexStr2ByteArray(data.target);
						g_leaf = data.leaf;
						g_teamName = data.teamName;

						g_waitingForData = false;

					}
				}
			});
		}

		if( currentTime - g_BlockUpdateTimer > (3 * 60) ){

			// Update block table

			g_BlockUpdateTimer = currentTime;

			g_blockTable.ajax.reload();
		}

		checkHashes();
	}
}


function checkHashes(){

	var secret_base = randomString(6);
	var hash_base = g_leaf+g_teamName+secret_base;
	var foundHash = false;

	if(!g_waitingForData){
		for(var h = 0; h < 10000; h++){
			g_hashesTotal++;

			var hash = asmCrypto.SHA256.bytes(hash_base+h.toString());

			if(hash.length === g_targetBytes.length){
				for(var b = 0, len = hash.length; b < len; b++){
					if(hash[b] > g_targetBytes[b]){
						break;
					}else if(hash[b] < g_targetBytes[b]){
						foundHash = true;
						
						var hashData = {
											target : g_targetStr,
											secret : secret_base+h.toString(),
											leaf : g_leaf
										};

						$.ajax({
							type: 'POST',
							url: "ajax/checkBlock.php",
							data: JSON.stringify(hashData),
							dataType: 'json'
						}).done(function( data ) {

							if(data){

								if(!data.completed){
									if(data.status === "success"){
										g_validHashes++;
										g_targetStr = data.new_target;
										g_targetBytes = hexStr2ByteArray(data.new_target);
										g_leaf = data.new_leaf;

										$("#current_target").html(g_targetStr);
										$("#current_leaf").html(g_leaf);
										$("#valid_hashes").html(g_validHashes.toString());

										// prevent unecessary data fetching
										g_DataUpdateTimer = new Date().getTime() / 1000;

										setTimeout(function(){ decryptLoop(); }, 1);
									}else if(data.status === "error"){
										
										// Trigger data update
										g_DataUpdateTimer = 0;

										// wait between 3 and 10 seconds to fetch new data
										var wait = Math.random() * (10-3) + 3; 
										setTimeout(function(){ decryptLoop(); }, Math.floor(wait * 1000) );
									}
								}else{
									stopDecryption();
									finishedDecryption();
								}
							}
						});

						break;
					}
				}
			}
		}
	}

	if(!foundHash){
		setTimeout(function(){ decryptLoop(); }, 1);
	}
}


// converts a hex string to a byte array
function hexStr2ByteArray(s){

	var arr = Array();

	for(var c = 0, len = s.length; c < len; c += 2){
		arr.push(parseInt(s.substring(c, c+2), 16));
	}

	return arr;
}


// returns a random string
function randomString(len){
	var retStr = "";
	var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for( var i = 0; i < len; i++ ){
		retStr += chars.charAt(Math.floor(Math.random() * chars.length));
	}

	return retStr;
}