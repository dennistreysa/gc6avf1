<?php

	session_start();
	session_unset();
	session_destroy();
	setcookie(session_name(), null, -1);

	// Delete cookie
	setcookie('SESSION_PERSISTENT', null, -1);

	header("Location: login.php");
?>