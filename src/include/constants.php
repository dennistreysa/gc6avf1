<?php
	
	// Teams
	define("TEAM_MIN_NAME_LENGHT", 3);
	define("TEAM_MAX_NAME_LENGHT", 25);
	define("TEAM_PASSWORD_LENGHT", 20);
	define("TEAM_MAX_ALLOWED_TEAMS", 1000);

	// Block stati
	define("BLOCK_STATUS_VALID", 1);
	define("BLOCK_STATUS_VALID_PENDING", 2);
	define("BLOCK_STATUS_TARGET_OUTDATED", 3);
	define("BLOCK_STATUS_LEAF_OUTDATED", 4);
	define("BLOCK_STATUS_BRANCH", 5);

	// Blocks table
	define("MAX_TEAM_BLOCKS_IN_TABLE", 50);

	// Stats
	define("TIME_DELTA_ONLINE", 300);

	// Check block
	define("MAX_SECRET_LENGTH", 30);
	define("BLOCKS_NEEDED", 1000);

	// Anonymous login
	define("ANONYMOUS_SESSION_STR_LEN", 20);
	define("ANONYMOUS_MAX_ALLOWED_SESSIONS", 10000);
	define("ANONYMOUS_COOKIE_SIGNATURE_LENGTH", 32);

	// Hash data
	define("HASH_STR_LENGTH", 64);

	// Recovery
	define("RECOVER_TOKEN_LENGTH", 32);
	define("RECOVER_COOLDOWN_MINUTES", 60);
	define("RECOVER_OUTDATE_HOURS", 24);

	// Cleanup
	define("CLEANUP_PROBABILITY", 5);	// 0 - 100 (0=never, 100=always)
	define("CLEANUP_LEAFS_THRESHOLD", 10000);

?>