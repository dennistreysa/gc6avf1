<?php

	require_once(__DIR__."/constants.php");
	require_once(__DIR__."/helper/rand.php");
	require_once(__DIR__."/Db.class.php");

	/*
	 *	Function to get the current leaf for a specific team
	 *
	 *	Note:
	 *		If the team has no own leaf a random leaf is returned
	 *
	 *	@param:
	 *		$teamID: The ID of the team for which the current leaf shall be returned
	 *
	 *	@return:
	 *		The current leaf
	 */
	function getCurrentLeaf($teamID, $database = null){

		// Backwards compatibility
		if(!$database){
			$database = new Db();
		}

		// Check if team has one of the last leafs
		$database->bind("teamID", $teamID);
		$database->bind("status_pending", BLOCK_STATUS_VALID_PENDING);
		$leaf = $database->query("SELECT	hash
										FROM leafs
										WHERE teamID = :teamID
											AND status = :status_pending
										LIMIT 1;");
		if(sizeof($leaf) == 1){
			return strtolower($leaf[0]["hash"]);
		}

		// Else return random leaf
		$database->bind("status_pending", BLOCK_STATUS_VALID_PENDING);
		$leaf = $database->query("SELECT	hash
										FROM leafs
										WHERE status = :status_pending
										ORDER BY RAND()
										LIMIT 1;");

		if(sizeof($leaf) == 1){
			return strtolower($leaf[0]["hash"]);
		}
		
		// hopefully this code is never reached...
		$randHash = strtolower(hash('sha256', generateRandomString() ));

		$database->bind("hash", $randHash);
		$database->bind("status", BLOCK_STATUS_VALID_PENDING);
		$leaf = $database->query("INSERT INTO	leafs
										(
											hash,
											parentID,
											targetID,
											teamID,
											secret,
											status
										)
										VALUES
										(
											:hash,
											0,
											(SELECT max(id) FROM targets),
											(SELECT min(id) FROM teams),
											'-exception_handling-',
											:status
										);");

		return $randHash;
	}


	/*
	 *	This function returns the current target
	 *
	 *	@return:
	 *		The current target
	 */
	function getCurrentTarget($database = null){

		// Backwards compatibility
		if(!$database){
			$database = new Db();
		}

		$target = $database->query("SELECT hash
										FROM targets
										WHERE id = (SELECT max(id) FROM targets)
										LIMIT 1;");

		return strtolower($target[0]["hash"]);
	}


	/*
	 *	This function returns the number of valid blocks of a team
	 *
	 *	@param:
	 *		$teamID
	 *
	 *	@return
	 *		The number of valid blocks
	 */
	function getValidBlocks($teamID, $database = null){
		
		// Backwards compatibility
		if(!$database){
			$database = new Db();
		}

		$database->bind("teamID", $teamID);
		$query = $database->query("SELECT	blocksAccepted
										FROM teams
										WHERE id = :teamID;");

		return intval($query[0]["blocksAccepted"]);
	}
?>