<?php

	/*
	 *	Generates a random string
	 *
	 *	@param
	 *		$length: length of generates string (default is 10)
	 *
	 *	@return:
	 *		The randoms string
	 *
	 *	credits: http://stackoverflow.com/questions/4356289/php-random-string-generator
	 */
	function generateRandomString($length = 10, $seed = null) {

		if($seed != null){
			srand($seed);
		}

		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

?>