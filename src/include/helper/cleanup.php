<?php

	require_once(__DIR__."/../Db.class.php");
	require_once(__DIR__."/../constants.php");

	function cleanUpDataBase($database = null) {

		if(!$database){
			$database = new Db();
		}

		if (rand (0, 100) <= CLEANUP_PROBABILITY) {
			// Check if database is 'full'
			$leafs = $database->query("	SELECT	count(*) AS numberOfLeafs
											FROM leafs;");

			if (intval($leafs[0]["numberOfLeafs"]) >= CLEANUP_LEAFS_THRESHOLD) {
				$maxBlocks = MAX_TEAM_BLOCKS_IN_TABLE;
				$validPending = BLOCK_STATUS_VALID_PENDING;

				// Get all teamIDs
				$database->bind("MAX_TEAM_BLOCKS_IN_TABLE", $maxBlocks);
				$teams = $database->query("	SELECT id
												FROM teams
												WHERE blocksSent >= :MAX_TEAM_BLOCKS_IN_TABLE;");

				foreach($teams as $counter => $team){
					$database->bind("teamID", $team["id"]);
					$database->bind("MAX_TEAM_BLOCKS_IN_TABLE", $maxBlocks);
					$database->bind("BLOCK_STATUS_VALID_PENDING_1", $validPending);
					$database->bind("BLOCK_STATUS_VALID_PENDING_2", $validPending);
					$cleanUpQuery = $database->query("	DELETE
															FROM leafs
															WHERE id NOT IN 
																(
																	SELECT id FROM
																		(
																			SELECT	id
																				FROM leafs
																				WHERE teamID = :teamID
																				ORDER BY id DESC
																				LIMIT :MAX_TEAM_BLOCKS_IN_TABLE
																		)A
																)
																AND id NOT IN
																(
																	SELECT * FROM
																		(
																			SELECT	parentID
																				FROM leafs
																				WHERE status = :BLOCK_STATUS_VALID_PENDING_1
																		)B
																)
																AND id NOT IN
																(
																	SELECT * FROM
																		(
																			SELECT	id
																				FROM leafs
																				WHERE status = :BLOCK_STATUS_VALID_PENDING_2
																		)B
																);");
				}
			}
		}
	}

?>