<?php
	
	require_once(__DIR__."/../Db.class.php");
	require_once(__DIR__."/validation.php");


	/*
	 *	This function returns the session string from a session cookie by removing the signature
	 *
	 *	@param:
	 *		$cookie: The cookie from which the session string should be extracted
	 *
	 *	@return:
	 *		The session string
	 */
	function getSessionStr($cookie){
		return explode('-', $cookie, 2)[0];
	}


	/*
	 *	This function restores a PHP-Session from a SESSION_PERSISTENT cookie.
	 *
	 *	Note:	
	 *		Make sure to call session_start() first to avoid unnecessary database queries.
	 *
	 *	@param:
	 *		$forceCheck: Load data from database nevertheless if $_SESSION variables are already set (default: false)
	 *		$getRealName: Inidcates if the realname of the team shall be fetched, else "-anonymous-" is used (default: false)
	 *		$createTeam: Inicates if a team shall be created. This is set to true the first time the team logs in.
	 *
	 *	@using:
	 *		$_COOKIE['SESSION_PERSISTENT']: The cookie from which the session will be restored
	 */
	function session_restore($forceCheck = false, $getRealName = false, $createTeam = false){
		
		// Check if session needs to be restored
		if(!isset($_SESSION["teamID"]) || $forceCheck){
			if(!empty($_COOKIE['SESSION_PERSISTENT'])){
				if(isValidSignature($_COOKIE['SESSION_PERSISTENT'])){
					// Restore session
					$database = new Db();

					$sessionStr = getSessionStr($_COOKIE['SESSION_PERSISTENT']);

					$database->bind("cookie", $sessionStr );
					$team = $database->query("SELECT	id,
														teamID
													FROM anonymous_sessions
													WHERE cookie = :cookie
														AND valid = 1
													LIMIT 1;");

					if(sizeof($team) == 1){
						
						$_SESSION["teamID"] = $team[0]["teamID"];
						$sessionID = $team[0]["id"];

						if($getRealName && ( intval($team[0]["teamID"]) > 0 || $createTeam ) ){
							
							$database->bind("teamID", $team[0]["teamID"] );
							$team = $database->query("SELECT	name
															FROM teams
															WHERE id = :teamID
															LIMIT 1;");
							
							if(sizeof($team) == 1){
								$_SESSION["name"] = $team[0]["name"];
							}else if($createTeam){
								$teamData = createNewAnonymousTeam($sessionID);
								$_SESSION["teamID"] = $teamData[0];
								$_SESSION["name"] = $teamData[1];
							}
						}else{
							$_SESSION["name"] = "-anonymous-";
						}
					}else{
						$_SESSION["teamID"] = null;
					}
				}
			}
		}
	}


	/*
	 *	Creates a random team with a random name and a random password.
	 *
	 *	@param:
	 *		$sessionID: The id of the session for which the cream shall be created
	 *
	 *	@return:
	 *		arr[0]: The id of the team created
	 *		arr[1]: The name of the team created
	 */
	function createNewAnonymousTeam($sessionID){

		$database = new Db();

		//Check if teamname already exits
		while(true){
			// Generate a team name
			$teamName = generateRandomString(TEAM_MAX_NAME_LENGHT);

			$database->bind("name", $teamName);
			$teams = $database->query("SELECT id FROM teams
											WHERE name = :name;");
			if(sizeof($teams) == 0){
				break;
			}
		}

		// Password will be lost, but we don't want an empty field ;)
		$teamPassword = password_hash(generateRandomString(TEAM_PASSWORD_LENGHT), PASSWORD_DEFAULT);

		$database->bind("name", $teamName);
		$database->bind("password", $teamPassword);
		$database->query("INSERT INTO teams
											(
												name,
												password,
												created,
												loggedIn,
												isAnonymous
											)
											VALUES
											(
												:name,
												:password,
												NOW(),
												1,
												1
											);");

		$teamID = $database->lastInsertId();

		// point session to new teamID
		$database->bind("sessionID", $sessionID);
		$database->bind("teamID", $teamID);
		$database->query("UPDATE	anonymous_sessions
								SET teamID = :teamID
								WHERE id = :sessionID;");

		return [$teamID, $teamName];
	}
?>