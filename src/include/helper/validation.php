<?php

	require_once(__DIR__."/../constants.php");

	
	/*
	 *	Check if the requests is a 'xmlhttprequest'
	 */
	function isXMLHTTPRequest(){
		return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
	}

	
	/*
	 *	Checks if a string is a valid JSON-Object
	 *
	 *	@param:
	 *		$string: the string to be checked
	 *
	 *	@return:
	 *		true|false
	 *
	 *	credits: http://subinsb.com/php-check-if-string-is-json
	 */
	function isJSON($string){
		return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
	}

	
	/*
	 *	Checks if a cookie is properly signed
	 *
	 *	@param:
	 *		$cookie: the cookie to be checked
	 *
	 *	@return:
	 *		true|false
	 */
	function isValidSignature($cookie){
		$valid = false;

		if(!empty($cookie)){

			$checkCookie = explode('-', $cookie, 2);

			if(sizeof($checkCookie) == 2 && strlen($checkCookie[1]) == ANONYMOUS_COOKIE_SIGNATURE_LENGTH){
				if( strtolower($checkCookie[1]) == strtolower(md5($checkCookie[0].get_cfg_var("bka.cfg.ANON_COOKIE_SALT"))) ){
					// Cookie valid
					$valid = true;
				}
			}
		}

		return $valid;
	}


	/*
	 *	This function checks if a string could be a valid team name by it's meta data
	 *
	 *	@param:
	 *		$name: The string to check
	 *
	 *	@return:
	 *		true|false
	 */
	function checkMetaTeamName($name){
		return !empty($name)
					&& is_string($name)
					&& strlen($name) >= TEAM_MIN_NAME_LENGHT
					&& strlen($name) <= TEAM_MAX_NAME_LENGHT
					&& !preg_match('/[^\x20-\x7e]/', $name);
	}


	/*
	 *	This function checks if a string could be a valid password by it's meta data
	 *
	 *	@param:
	 *		$password: The string to check
	 *
	 *	@return:
	 *		true|false
	 */
	function checkMetaPassword($password){
		return !empty($password)
					&& is_string($password)
					&& strlen($password) == TEAM_PASSWORD_LENGHT;
	}


	/*
	 *	This function checks if a string could be a valid hash by it's meta data
	 *
	 *	@param:
	 *		$target: The string to check
	 *
	 *	@return:
	 *		true|false
	 */
	function checkMetaHash($hash){
		return !empty($hash)
					&& is_string($hash)
					&& strlen($hash) == HASH_STR_LENGTH;
	}


	/*
	 *	This function checks if a string is a valid secret
	 *
	 *	@param:
	 *		$secret: The string to check
	 *
	 *	@return:
	 *		true|false
	 */
	function isValidSecret($secret){
		return !empty($secret)
					&& is_string($secret)
					&& strlen($secret) <= MAX_SECRET_LENGTH
					&& !preg_match('/[^\x21-\x7e]/i', $secret);
	}
?>