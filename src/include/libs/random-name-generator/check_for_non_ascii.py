import re

with open("video_game_names.txt", "r") as checkFile:
	for l in checkFile:
		no_ascii = re.findall(r"[^\x00-\x7F]", l)

		if no_ascii:
			print(l)
			for index, c in enumerate(l):
				if ord(c) > 0x7f:
					print(index)
